<?php get_header() ?>
    <div class="sobre-main">
        <div class="content">
            <div class="left-side">
                <div class="half-container">
                    <p class="sub-title azul">Conheça sobre a gente</p>
                    <h2 class="title">O bluemoon</h2>
                    <div class="text">
                        <p>
                            Somos mais que um motel. Somos um espaço envolvente que busca 
                            oferecer o melhor momento àqueles que amam surpreender, agregando
                            a este princípio o máximo de conforto, segurança e privacidade.
                        </p> <br>
                        <p>
                            Ao longo do tempo, buscamos inspiração em diversos tipos de ambiente, trazendo a 
                            essência encontrada para as nossas suítes temáticas, feitas para os que amam usar a 
                            criatividade e possuem o desejo de transformar momentos íntimos em algo inesquecível.
                        </p>   
                    </div>
                </div>
            </div>
            <div class="right-side">
                <img src="<?= get_image_url("SPA.webp")?>"  alt="SPA Do Motel Bluemoon">
            </div>
        </div>
        <div class="second-c">
            <div class="right-side img">
                <img src="<?= get_image_url("suite.webp")?>" alt="Suíte Do Motel Bluemoon">
            </div>
            <div class="left-side text">
                <div class="half-container">
                    <div class="text">
                        <p>
                            Nossas suítes possuem características distintas, cada uma tem o seu charme. São suítes duplex, standard, com piscina aquecida, 
                            hidromassagem, pole dance, churrasqueira, duas camas e por aí vai. Todas prontas para atender qualquer tipo de gosto.
                        </p> <br>
                        <p>
                            Portanto, seja para uma passadinha rápida ou uma noite completa, o que queremos proporcionar a você é uma experiência única!
                        </p> 
                    </div>
                </div>
            </div>
        </div>
        <div class="image-duvidas">
            <div class="banner" style="background-image: url('<?= get_field("banner_pagina_sobre")['url']?>');">
                <div class="left-side">
                    <div class="text-whiteplan">
                        <p class="banner-title">primeira vez no blue?</p>
                        <p class="banner-text">Preparamos uma lista com as dúvidas mais frequentes sobre o nosso funcionamento.</p>
                    </div>
                </div>
                <div class="right-side">
                    <a href="<?= home_url()?>/duvidas/"><button id="duvidas" class="btn-whiteplan">Confira!</button></a>    
                </div>
            </div>
        </div>
    </div>
<?php get_footer() ?>