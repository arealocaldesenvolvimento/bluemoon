<?php get_header() ?>
    <div class="al-container duvidas-main">
        <div class="duvidas">
            <div class="titulo-main">
                <h2 class="sup">Tire suas</h2>
                <h1 class="sub">Dúvidas</h1>
                <p class="legend">Muitas dúvidas surgem quando o assunto é motel. Por isso, preparamos um "perguntas e respostas" 
                    com as dúvidas mais frequentes sobre nosso funcionamento.</p>
            </div>
            <ul class="duvidas-lista">
                <?php
                        foreach(get_field("duvidas") as $duvida):
                            ?>   
                            <li class="duvida">
                                <h2 class="subtitle"><?= $duvida['titulo_da_duvida']?></h2>
                            </li>
                            <span class="subtext"><?= $duvida['descricao_da_duvida']?></span>
                            <?php
                        endforeach;
                ?>
            </ul>
        </div>
    </div>
<?php get_footer() ?>