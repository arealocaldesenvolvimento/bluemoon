    </div>

    <!-- Footer -->
    <footer class="main-footer">
        <div class="al-container">
            <div class="row-footer">
                <div class="logo column-footer">
                    <a title="Motel Bluemoon">
                        <a href="<?= get_site_url() ?>"><img src="<?= get_field('logomarca_footer', 'option')['url'] ?>" alt="Logo Motel Bluemoon"></a><br>
                        <span class="direito">
                            <p>todos os direitos reservados<br>hotel bluemoon</p>
                        </span>
                    </a>
                </div>
                <div class="contato column-footer">
                    <p class="footer-title">Contato</p>
                    <p class="redirect" id="telefone">
                        <a href="tel:+<?= get_field('telefone', 'option') ?>" title="Telefone Bluemoon">
                        <img src="<?= get_image_url("phone-call.png") ?>">
                        <span><?= get_field('telefone', 'option') ?></span>
                        </a>
                    </p>
                    <p class="redirect" id="e-mail">
                        <a href="mailto:<?= get_field('email', 'option') ?>" title="E-mail Bluemoon">
                        <img src="<?= get_image_url("mail.png") ?>">
                        <span><?= get_field('email', 'option') ?></span>
                        </a>
                    </p>
                </div>
                <div class="localizacao column-footer">
                    <a>
                        <p class="footer-title">Nossa localização</p>
                        <div>
                            <a href="https://goo.gl/maps/RPDVamcuPyXFFMvJ9" target="_blank" class="conteudo redirect" id="localizacao">
                            <img src="<?= get_image_url("place.png") ?>"><br>
                            <span>BR-470, s/n - Rainha, <br>Rio do Sul - SC, 89160-000</span></a>
                        </div>
                    </a>
                </div>
                <div class="redes-sociais column-footer">
                    <p class="footer-title">Siga Nossas<br> Redes Sociais</p>
                    <p>
                        <a href="<?= get_field('facebook-link', 'option') ?>" target="_blank" class="content-redes-sociais to-facebook" title="Instagram Facebook">
                            <img id="facebook" class="redirect" src="<?= get_image_url("facebook.png") ?>">
                        </a> 
                        <a href="<?= get_field('instagram-link', 'option') ?>" target="_blank" class="content-redes-sociais to-instagram" title="Instagram Instagram">
                        <img id="instagram" class="redirect" src="<?= get_image_url("instagram.png") ?>">
                        </a>
                    </p>
                </div>
            </div>
        </div>
        <div class="dev">
            <div><a href="https://arealocal.com.br/" target="_blank" title="Link Website Área Local">Desenvolvido por: Área Local</a></div>
        </div>    
    </footer>
        
    <!-- Scripts -->    
    <script>
        /**
         * @description global JS variables
         */
        window.alUrl = {
            templateUrl: '<?php echo addslashes(get_bloginfo('template_url')); ?>',
            homeUrl: '<?php echo addslashes(home_url()); ?>'
        }
	    window.apiUrl = `${window.alUrl.homeUrl}/wp-json/api`
    </script>
    <script src="https://kit.fontawesome.com/4800576786.js" crossorigin="anonymous"></script>

    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/public/js/vendor.js"></script>
    <script async type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/public/js/app.js"></script>
    <?php wp_footer(); ?>
</body>
</html>
