<?php get_header() ?>
    <div class="al-container reservas-main">
        <div class="titulo-main">
            <h2 class="sup">Faça sua</h2>
            <h1 class="sub">Reserva</h1>
            <div class="text">
                <p>Sexta, sábado e domingo, reserva somente para pernoite. Para confirmar a reserva da suíte desejada,
                     pedimos que faça sua solicitação com 72 horas de antecedência.
                </p>
                <p>
                    A reserva somente será confirmada após
                    comprovação do pagamento. Em caso de
                    dúvidas, entre em contato.
                </p>
                <p class="especial"> 
                    As reservas podem ser feitas através dos seguintes canais:
                </p>
                <?php
                    $url=get_field("banner_pagina_reservas")['url'];
                    $contatos = new WP_Query(array('post_type' => 'post'));
                    $contatos->the_post(); 
                ?>
                <p class="text-icon">
                    <a href="tel:+<?= get_field('telefone', 'option') ?>" >  
                        <img class="icon-especial" src="<?= get_image_url("phone-call-blue.png") ?>">
                        <span><?= get_field('telefone', 'option') ?></span>
                    </a>
                </p>
                <p class="text-icon">
                    <a href="mailto:<?= get_field('email', 'option') ?>">
                    <img class="icon-especial mail" src="<?= get_image_url("mail-blue.png") ?>">
                    <span><?= get_field('email', 'option') ?></span>
                    </a>
                </p>
            </div>
            <div class="rowW">
                <div class="image-whats" style="background-image: url('<?= $url ?>;');">
                    <div class="left-side">
                        <p class="text-whats">Reserve pelo <br><span class="texto-estilizado">Whatsapp</span></p>
                    </div>
                    <div class="right-side">
                        <a href="https://wa.me/<?= get_field('whatsapp', 'option') ?>" target="_blank"><button class="btn-whats">chamar!</button></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="contato">
            <?= do_shortcode('[contact-form-7 id="204" title="Contato 1"]') ?>
        </div>
    </div>
    <div class="map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3549.2899450229197!2d-49.59640688334837!3d-27.17862247534019!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94eb4363861aefd3%3A0x70e9970d8982ca91!2sMotel%20Blue%20Moon!5e0!3m2!1spt-BR!2sbr!4v1642017969117!5m2!1spt-BR!2sbr"   
        width="100%" height="440px" allowfullscreen="allowfullscreen"></iframe>
    </div>
<?php get_footer() ?>