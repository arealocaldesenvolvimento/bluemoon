/**
 * Global theme functions
 */

/**
 * @description Exemplo ajax
 */
const exampleAjax = async () => {
	const stateResponse = await fetch(`${window.apiUrl}/estados`)
	const { states, message } = await stateResponse.json()
	console.log(states, message)
}

window.onload = async () => {
	/**
	 * Contact form 7 alerts
	 */
	const form = document.querySelector('.wpcf7')
	if (form) {
		form.addEventListener('wpcf7mailsent', () => {
			Swal.fire({
				icon: 'success',
				title: 'Sucesso!',
				text: 'Mensagem enviada!',
			})
		})

		form.addEventListener('wpcf7mailfailed', () => {
			Swal.fire({
				icon: 'error',
				title: 'Ocorreu um erro!',
				text: 'Se o erro persistir, favor contatar o suporte.',
			})
		})
	}
}
