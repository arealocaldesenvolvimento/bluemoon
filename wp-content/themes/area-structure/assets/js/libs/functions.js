$(document).ready(()=>{
    createSlider();
    $("#duvidas").on("click", ()=>{
        window.open(window. location.href+"/duvidas/", "_self");
    });
    startHomeSlider();
    $("#loading").hide()
    $(".btn-enviar").on("click", ()=>{          
        $("#loading").show()
    })
    document.addEventListener( 'wpcf7mailsent', ()=> {
        $("#loading").hide()
    }, false );
    document.addEventListener( 'wpcf7invalid', ()=> {
        $("#loading").hide()
    }, false );
    document.addEventListener( 'wpcf7mailfailed', ()=> {
        $("#loading").hide()
    }, false );
});
function createSlider(){
    var sliderSingle = $('.slider-suites-single').lightSlider({
        item: 1,
        autoWidth: false,
        slideMove: 1, 
        slideMargin: 0,
        speed: 400, 
        auto: false,
        loop: true,
        slideEndAnimation: true,
        pause: 2000,
        keyPress: false,
        controls: true,
        prevHtml: '<i class="left-png"><img src="'+window.alUrl.homeUrl+'/wp-content/themes/area-structure/assets/images/single-prev.png"></i>',
        nextHtml: '<i class="right-png"><img src="'+window.alUrl.homeUrl+'/wp-content/themes/area-structure/assets/images/single-next.png"></i>',
        pager: false
    });
    var sliderList = $('.slider-suites').lightSlider({
        item: 1,
        autoWidth: false,
        slideMove: 1, 
        slideMargin: 0,
        speed: 400, 
        auto: false,
        loop: true,
        slideEndAnimation: true,
        pause: 2000,
        keyPress: false,
        controls: true,
        prevHtml: '<i class="left-png"><img src="'+window.alUrl.homeUrl+'/wp-content/themes/area-structure/assets/images/single-prev.png"></i>',
        nextHtml: '<i class="right-png"><img src="'+window.alUrl.homeUrl+'/wp-content/themes/area-structure/assets/images/single-next.png"></i>',
        pager: true
    });
}
function alterar(id, comand){
    
    var max = parseInt($(".rowt").length);
    for(var i = 1; i <max; i++){
        $("#cod-"+i).hide();
    }
    
    if(comand==1){
        if(id==max){
            $("#cod-"+id).hide();
            $("#cod-1").show();
        }else{
            $("#cod-"+id).hide();
            $("#cod-"+(id+1)).show();
        } 
    }else{
        if(id==1){
            $("#cod-"+id).hide();
            $("#cod-"+max).show();
            $("#cod-"+max).show();
        }else{
            $("#cod-"+id).hide();
            $("#cod-"+(id-1)).show();
        }
    }
}
function startHomeSlider(){
    var max = parseInt($(".rowt").length);
    for(var i = 2; i <=max; i++){
        $("#cod-"+i).hide();
    }  
}