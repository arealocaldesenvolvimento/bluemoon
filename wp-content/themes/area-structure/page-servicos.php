<?php get_header() ?>
    <div class="al-container servicos-main">
        <div class="titulo-main">
            <h2 class="sup">Confira nossos</h2>
            <h1 class="sub">Serviços</h1>
        </div>
        <div class="servicos-page">
            <?php
            foreach(get_field("servicos") as $chave => $valor):
                if($chave%2===0):
                ?>
                <div class="left">    
                    <span class="background-text"><?= ($chave<9?'0':'' ).($chave+1) ?></span>
                    <div class="subtitle"><?= $valor['titulo'] ?></div>
                    <span class="legend"><?= $valor['legenda'] ?></span>
                </div>
                <?php
                else:
                ?>
                <div class="right">   
                    <span class="legend"><?= $valor['legenda'] ?></span> 
                    <span class="background-text"><?= ($chave<9?'0':'' ).($chave+1) ?></span><br>
                    <div class="subtitle"><?= $valor['titulo'] ?></div>
                </div>
                <?php
                endif;
                if($chave+1==count(get_field("servicos"))/2):
                ?>
        </div>
    </div>
        <div class="imagem-meio">
            <div class="img-servico" style="background-image: url('<?= get_field("banner_central")['url']?>');">          
                <div class="left-side">
                    <p class="banner-text">Saia da rotina</p><p class="banner-text broke">com segurança!</p>
                </div>
                <div class="right-side">
                    <button onclick="window.location.href='<?= home_url()?>/reservas/'" class="btn-reserva" id="faca-reserva">Faça sua reserva</button>
                </div>
            </div>
        </div>
        <div class="al-container servicos-main">
            <div class="servicos-page">     
            <?php
                endif;
            endforeach;
            ?>
            </div>
        </div>
<?php get_footer() ?>