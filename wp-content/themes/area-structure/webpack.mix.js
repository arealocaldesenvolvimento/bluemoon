/**
 * Modules
 */
const mix = require('laravel-mix')

/**
 * Project proxy
 */
const proxy = 'http://localhost/your-site-dir'

mix
	/**
	 * SCSS/SASS transpiler
	 */
	.sass('assets/scss/index.scss', 'style.css')
	/**
	 * JS transpiler
	 */
	.scripts(['assets/js/libs/*.js'], 'public/js/vendor.js')
	/**
	 * JS watcher
	 */
	.js(['assets/js/app.js', 'assets/js/theme/functions.js'], 'public/js')
	/**
	 * Configs
	 */
	.options({ processCssUrls: false })
	.browserSync(proxy)
	.disableNotifications()
	.setPublicPath('')
	.webpackConfig({
		output: {
			chunkFilename: 'public/chunks/[name].js',
		},
	})
