<?php get_header(); ?>
    <div class="al-container">
        <div class="titulo-main">
            <h2 class="sup">Veja nossas</h2>
            <h1 class="sub">Suítes</h1>
        </div>
    </div>
    <div class="suites-main">
        <div class="suites-archive">
            <?php
                $suites = new WP_Query(array(
                   'post_type' => 'suites',
                   'posts_per_page'	=> -1,
                   'meta_key' => 'numero',
                   'orderby' => 'meta_value_num',
                   'order' => 'ASC'
                ));
                $idP=0;
                while($suites->have_posts()):
                    $suites->the_post(); 
                    ?>   
                    <div class="suite-i">
                        <?php if($idP%2===0): ?>
                        <div class="left-s first">
                            <div class="nome-num">
                                <h2 class="title-background"><?= get_field('numero')?></h2>
                                <h2 class="nome"><?= get_the_title()?></h2>
                                <div class="valores-horas">
                                    <p class="val"><span class="real">R$</span><span class="azul title2"><?= str_replace(".", ",", get_field("valor_2h")) ?></span><br>
                                    <span class="sublegend">2 horas</span></p>
                                </div>
                                <div class="reservas">
                                    <a href="<?= get_page_link() ?>"><button class="btn-reserva" id="faca-reserva">Conheça o quarto</button></a>
                                </div>
                            </div>
                        </div>
                        <div class="right-s second">   
                            <div class="imagem-suites">
                                <?php
                                if(get_field("galeria")[0]['url']!=null){    
                                    ?>
                                    <div class="slide-show">
                                        <ul class="slider-suites" id="id-slider-<?= $idP?>">
                                            <?php
                                                if(get_field('galeria')):
                                                    foreach(get_field('galeria') as $imagem):
                                            ?>
                                                <li data-thumb="<?= $imagem['url'] ?>" data-src="<?= $imagem['url'] ?>">
                                                    <img src="<?= $imagem['url'] ?>" alt="<?= $imagem['title'] ?>" height="420px"> 
                                                </li>
                                            <?php
                                                    endforeach;
                                                endif;
                                            ?>
                                        </ul>
                                    </div>
                                    <?php
                                }else{
                                ?>
                                    <i class="far fa-images"></i>
                                <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div> 
                    <?php else: ?>     
                        <div class="left-s second">   
                            <div class="imagem-suites">
                                <?php
                                if(get_field("galeria")[0]['url']!=null){    
                                    ?>
                                    <div class="slide-show">
                                        <ul class="slider-suites" id="id-slider-<?= $idP?>">
                                            <?php
                                                if(get_field('galeria')):
                                                    foreach(get_field('galeria') as $imagem):
                                            ?>
                                                <li data-thumb="<?= $imagem['url'] ?>" data-src="<?= $imagem['url'] ?>">
                                                    <img src="<?= $imagem['url'] ?>" alt="<?= $imagem['title'] ?>" height="420px"> 
                                                </li>
                                            <?php
                                                    endforeach;
                                                endif;
                                            ?>
                                        </ul>
                                    </div>
                                <?php
                                }else{
                                ?>
                                    <i class="far fa-images"></i>
                                <?php
                                }
                                ?>
                            </div>
                        </div>
                        <div class="right-s first">
                            <div class="nome-num">
                                <h2 class="nome"><b><?= get_the_title()?></b></h2>
                                <h2 class="title-background"><?= get_field('numero')?></h2>
                                <div class="valores-horas">
                                    <p class="val"><span class="real">R$</span><span class="azul title2"><?= str_replace(".", ",", get_field("valor_2h")) ?></span><br><span class="sublegend">2 horas</span></p>
                                </div>
                                <div class="reservas">
                                    <a href="<?= get_page_link() ?>"><button class="btn-reserva" id="faca-reserva">Conheça o quarto</button></a>
                                </div>
                            </div>
                        </div>           
                    </div>
                        <?php
                            endif;
                        $idP++;
                endwhile;
                        ?>
                    </div>
                </div>
<?php get_footer() ?>