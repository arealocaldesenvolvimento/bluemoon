<?php get_header() ?>
    <div class="slider-home">
        <?= do_shortcode('[rev_slider alias="slide-home"][/rev_slider]') ?>
    </div>
    <div class="slider-home-mobile">
        <?= do_shortcode('[rev_slider alias="slider-1"][/rev_slider]') ?>
    </div>
    <div class="content">
        <div class="left-side">
            <div class="half-container">
                <p class="sub-title azul">Conheça sobre a gente</p>
                <h2 class="title">O bluemoon</h2>
                <div class="text">
                    <p>
                        Somos mais que um motel. Somos um espaço envolvente que busca 
                        oferecer o melhor momento àqueles que amam surpreender, agregando
                        a este princípio o máximo de conforto, segurança e privacidade.
                    </p> <br>
                    <p>
                        Ao longo do tempo, buscamos inspiração em diversos tipos de ambiente, trazendo a 
                        essência encontrada para as nossas suítes temáticas, feitas para os que amam usar a 
                        criatividade e possuem o desejo de transformar momentos íntimos em algo inesquecível.
                    </p>   
                </div>
                <a href="<?= home_url()?>/sobre/"><button id="conheca-sobre" class="btn"><b>CONHEÇA MAIS!</b></button></a>
            </div>
        </div>
        <div class="right-side">
            <img src="<?= get_image_url("SPA.webp")?>"  alt="Suíte Do Motel Bluemoon">
        </div>
    </div>
    <div class="suites">
            <?php
                $suites = new WP_Query(array(
                    'post_type' => 'suites',
                    'posts_per_page'	=> -1,
                    'meta_key' => 'numero',
                    'orderby' => 'meta_value_num',
                    'order' => 'ASC',
                ));
                $cont=0;
                while($suites->have_posts()):
                    $cont++;
                    $suites->the_post(); 
                    ?>   
                    <div class="rowt" id="cod-<?= $cont?>">
                        <div class="nome-num">
                            <h2 class="nome"><b><?= get_the_title()?></b></h2>
                            <h2 class="title-background"><?= get_field('numero')?> </h2>
                            <div class="valores-horas">
                                <p class="val"><span class="azul title2"><span class="real">R$</span><?= str_replace(".", ",", get_field("valor_2h")) ?></span><br><span class="sublegend">2 horas</span></p>
                            </div>
                                <div class="reservas">
                                    <button onclick="window.location.href='<?= get_page_link()?>'" class="btn-reserva" id="faca-reserva">Conheça o quarto</button>
                            </div>
                            <div class="buttons">
                                <a class="back" onclick="alterar(<?= $cont ?>, 2)"><i class="right-png"><img src="<?= get_image_url("prev.png")?>"></i></a>
                                <a class="next" onclick="alterar(<?= $cont ?>, 1)"><i class="right-png"><img src="<?= get_image_url("next.png")?>"></i></a>
                            </div>
                        </div>
                        <div class="img">
                            <?php
                                if(get_field("galeria")[0]['url']!=null){    
                                    ?>
                                    <img src="<?= get_field("galeria")[0]['url']?>" alt="<?= get_field("galeria")[0]['title'] ?>" class="imagem-suite">
                                    <?php
                                }else{
                                ?>
                                    <i class="far fa-images"></i>
                                <?php
                                }
                                ?>
                        </div>
                    </div>
                        <?php
                            endwhile;
                        ?>
    </div>
<div class="al-container">
    <div class="row cards">
        <div class="card-left">
            <img src="<?= get_image_url("cartao-bluemoon.png")?>" alt="Cartão Diamante Bluemoon">
            <div class="content">
                <div class="ind-txt">
                    <p class="sub-title azul">garanta seu</p>
                    <h2 class="title">desconto</h2>
                    <div class="text">
                        <p>Curta tudo o que há de melhor e ainda ganhe 15% de desconto com o nosso Cartão Diamante.
                        Solicite o seu com a recepção.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-right">
            <img src="<?= get_image_url("cloche.png")?>" alt="Cloche de metal">
            <div class="content">
                <div class="ind-txt">
                    <p class="sub-title azul">Conheça sobre a gente</p>
                    <h2 class="title">O bluemoon</h2>
                    <div class="text">
                        <p>Oferecemos lanches, bebidas e outras utilidades.</p>
                    </div>
                    <a href="<?= home_url()?>/sobre/"><button id="conheca-sobre" class="btn"><b>CONHEÇA MAIS!</b></button></a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row cafe">
    <div class="left">
        <img src="<?= get_image_url("cafe.png")?>" alt="Imagem de café" class="cafe-img">
    </div>
    <div class="right">
        <p class="sub-title azul">Servimos</p>
        <h2 class="title">Café da manhã</h2>
        <div class="text">
            <p>Servimos lanches em todos os horários. No caso de pernoite/perdia com reserva, o café da manhã para duas pessoas é grátis. Para outros tipos de refeição, solicite informação com a recepção.</p>
        </div>
        <a href="<?= home_url()?>/reservas/"><button class="btn-reserva" id="faca-reserva">Faça sua reserva</button></a>
    </div>
</div>
<?php get_footer() ?>