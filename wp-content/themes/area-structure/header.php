<!DOCTYPE html>
<html <?php language_attributes(); ?> xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="X-UA-Compatible" content="IE=7">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="web_author" content="Área Local">
    <meta name="theme-color" content="#000000">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?php image_url('favicon.png'); ?>">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <link
        rel="stylesheet"
        type="text/css"
        media="all"
        href="<?php echo get_template_directory_uri(); ?>/style.css"
    >
    <title>
        <?php echo is_front_page() || is_home() ? get_bloginfo('name') : wp_title(''); ?>
    </title>
    <?php wp_head(); ?>

    <link rel="stylesheet" type="text/css" href="<?= get_template_directory_uri() ?>/assets/css/libs/lightslider.css">
    <link rel="stylesheet" type="text/css" href="<?= get_template_directory_uri() ?>/assets/css/libs/lightgallery.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="<?= get_template_directory_uri() ?>/assets/js/libs/lightslider.js"></script>
    <script src="<?= get_template_directory_uri() ?>/assets/js/libs/lightslider.js"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-WT67GE238C"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-WT67GE238C');
    </script>
</head>
<body <?php body_class(); ?>>
    <!-- Header -->
    <header role="heading" class="main-header">
        <div class="al-container">
            <div class="logo">
                <a title="Motel Bluemoon" href="<?= get_site_url() ?>">
                    <img src="<?= get_field('logomarca', 'option')['url'] ?>" alt="Logo Motel Bluemoon">
                </a>
            </div>
            <div class="main-menu">
                <?php wp_nav_menu(array('menu'=> 'main-menu', 'theme_location' => 'principal', 'container' => false)); ?>
                <div class="button">
                    <a href="https://api.whatsapp.com/send/?phone=<?= get_field('whatsapp', 'option') ?>&text&app_absent=0" target="_blank">Reservas</a>
                </div>
            </div>
        </div>
    </header>
    <!-- Wrapper -->
    <div id="wrapper">
