<?php get_header() ?>
    <div class="single-slide">
        <ul id="adaptive" class="slider-suites-single">
            <?php
                if(get_field('galeria')):
                    foreach(get_field('galeria') as $imagem):
            ?>
                <li data-thumb="<?= $imagem['url'] ?>" data-src="<?= $imagem['url'] ?>">
                    <div style="background-image: url('<?= $imagem['url'] ?>');"></div>
                </li>
            <?php
                    endforeach;
                endif;
            ?>
        </ul>
    </div>
    <div class="al-container">
        <div class="suite">
            <div class="rowt">
                <div class="nome-num">
                    <h2 class="nome"><b><?= get_the_title()?></b></h2>
                    <h2 class="title-background"><?= get_field('numero')?> </h2>
                </div>
                <div class="info">
                    <h2 class="subtitle azul"><b>Informações da suíte</b></h2>
                    <div class="info-val">
                    <?php
                        if(get_field('informacoes_extra')):
                            echo '<div class="info-val01">';
                            for($i=0; $i <count(get_field('informacoes_extra'))/2;$i++):
                                ?>
                                    <p><?= get_field('informacoes_extra')[$i]['informacao']?></p>
                                <?php
                            endfor;
                            echo '</div>';
                            echo '<div class="info-val02">';
                            for(; $i < count(get_field('informacoes_extra'));$i++):
                                ?>
                                    <p><?= get_field('informacoes_extra')[$i]['informacao']?></p>
                                <?php
                            endfor;
                            echo '</div>';
                        endif;
                    ?> 
                    </div>

                </div>
            </div>
            
        </div>
        <div class="valores">
            <div>
                <h2 class="title2">Valores</h2>
                <div class="valores-suites">
                    <div class="valores-horas">
                        <h3 class="legend">2 horas</h3>
                        <p class="val"><span class="real">R$</span><span class="azul title3"><?= str_replace(".", ",", get_field("valor_2h")) ?></span></p>
                    </div>
                    <div class="valores-horas">
                        <h3 class="legend">Hora Extra</h3>
                        <p class="val"><span class="real">R$</span><span class="azul title3"><?= str_replace(".", ",", get_field("valor_hora_extra")) ?></span></p>
                    </div>
                    <div class="valores-horas">
                        <h3 class="legend">Pernoite</h3>
                        <p class="val"><span class="real">R$</span><span class="azul title3"><?= str_replace(".", ",", get_field("pernoite_domingo_a_quinta")) ?><br></span><span class="sublegend">Dom a Qui</span></p>
                    </div>
                    <div class="valores-horas">
                        <h3 class="legend">Pernoite</h3>
                        <p class="val"><span class="real">R$</span><span class="azul title3"><?= str_replace(".", ",", get_field("pernoite_sexta_a_sabado")) ?></span><br><span class="sublegend">Sex e Sab</span></p>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="reservas">
            <button onclick="window.open('https:\/\/api.whatsapp.com/send/?phone=<?= get_field('whatsapp', 'option') ?>')" class="btn-reserva" id="faca-reserva">Faça sua reserva</button>
        </div>
    </div>
<?php get_footer() ?>