<?php

/**
 * Imports.
 */
include_once ABSPATH.'/wp-load.php';

/**
 * Fetch all cities based on state id.
 */
function getCities(WP_REST_Request $request): WP_REST_Response
{
    global $wpdb;
    $stateId = $request->get_params()['estado'];

    try {
        $cities = $wpdb->get_results(
            '
			SELECT id, nome FROM al_cidades WHERE estado LIKE '.$stateId.'
			ORDER BY al_cidades.capital IS NULL, nome ASC'
        );

        $cities = json_encode($cities);
        $cities = preg_replace_callback(
            '/\\\\u([0-9a-fA-F]{4})/',
            function ($match) {
                return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UTF-16BE');
            },
            $cities
        );

        if (!empty($cities)) {
            return new WP_REST_Response([
                'status' => 200,
                'cities' => $cities,
                'message' => 'Cidades obtidas com sucesso',
            ]);
        }

        return new WP_REST_Response([
            'status' => 500,
            'message' => 'Erro ao obter as cidades',
        ]);
    } catch (\Throwable $th) {
        return new WP_REST_Response([
            'status' => 500,
            'message' => 'Um erro inesperado ocorreu: '.$th->getMessage(),
        ]);
    }
}
