<?php

flush_rewrite_rules();

/**
 * Example post-type.
 */

function type_post_suites() {
    $labels = array(
        'name' => _x('Suítes', 'post type general name'),
        'singular_name' => _x('Suite', 'post type singular name'),
        'add_new' => _x('Adicionar Novo', 'Novo item'),
        'add_new_item' => __('Novo Suite'),
        'edit_item' => __('Editar Suite'),
        'new_item' => __('Novo Suite'),
        'view_item' => __('Ver Suite'),
        'search_items' => __('Procurar Suítes'),
        'not_found' =>  __('Nenhuma Suite encontrado'),
        'not_found_in_trash' => __('Nenhum Suite encontrado na lixeira'),
        'parent_item_colon' => '',
        'menu_name' => 'Suítes'
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'public_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'menu_position' => 4,
        'menu_icon' => 'dashicons-admin-home',
        'supports' => array('title','editor','thumbnail','custom-fields', 'revisions')
    );

    register_post_type( 'suites' , $args );
}
add_action('init', 'type_post_suites');