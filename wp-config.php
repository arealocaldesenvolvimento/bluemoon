<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'motel_bluemoon' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', 'root' );

/** Nome do host do MySQL */
define( 'DB_HOST', '192.168.0.39' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define( 'DB_COLLATE', '' );

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'PFN4jm&1J=3p*^/w^hJdq{MN{k{<:-t;fgJnuqOMy!L[ln1QhM5VOS?Odt^%`%ON' );
define( 'SECURE_AUTH_KEY',  '_CFO~A9fk_y<37s;b4@/SkD9*4T2RBe<^{2k=6T,JH}R6]b6C/&n]r:|9?kP]@G$' );
define( 'LOGGED_IN_KEY',    'iC8k%HfcqnhwwvZ I)0]8>is]GoK_W9cdLDs+!Jslaf!u.Q~c=k7/w0q&*);hV?q' );
define( 'NONCE_KEY',        'nzlFX+7^@|vEJ*MIahu0S9mw95D4m!Hk`H[d9LNJ+]-iadu:bB#J{C>0obWUak*<' );
define( 'AUTH_SALT',        'kb2eGd)9S8wA4diI`.{]ht[]W7)+ d{qe^5N%>+sF[V$F=WpVV}^D9)?Ed~#c)rH' );
define( 'SECURE_AUTH_SALT', 'e5fBcOXT=TTh[BuAD7&]h CIm74hv}.>?wC/?~.gWEZ+6o-<uxubKc9^8d/ig~VN' );
define( 'LOGGED_IN_SALT',   'B**iUa&3^o?f;tTFT|)AS_(yRe(, a)kh*PD(/O3ZMwt.J6~HX3r$?A+,$Wi? 45' );
define( 'NONCE_SALT',       'PWHn/OXg[&7=?+F^8&~6y}w4nWaSx`5$-bbmZ#QLo5&F`p([x!oQpkx-JH>sX/([' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'al_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Configura as variáveis e arquivos do WordPress. */
require_once ABSPATH . 'wp-settings.php';
